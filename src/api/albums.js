angular
  .module('photo')
  .factory('Albums', Albums);

Albums.$inject = ['$resource', 'AppConfig'];

function Albums($resource, AppConfig) {
  return $resource(
    AppConfig.apiPath + '/albums/:slug/',
    {slug: '@slug'},
    {
      update: {
        method: 'PUT'
      },
      getAll: {
        method: 'GET'
      },
      getAlbum: {
        method: 'GET',
        isArray: true
      },
      createAlbum: {
        method: 'POST'
      },
      removeItem: {
        method: 'DELETE'
      }
    })
}



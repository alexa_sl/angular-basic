angular
  .module('photo')
  .factory('Photos', Photos);

Photos.$inject = ['$resource', 'AppConfig'];

function Photos($resource, AppConfig) {
  return $resource(
    AppConfig.apiPath + '/photos/:url/',
    {url: '@url'},
    {
      update: {
        method: 'PUT'
      },
      removeItem: {
        method: 'DELETE'
      }
    })
}



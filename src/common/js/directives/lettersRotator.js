(function () {
  'use strict';

  angular
    .module('photo')
    .directive('lettersRotator', lettersRotatorDirective);

  lettersRotatorDirective.$inject = ['$interval', '$timeout'];

  function lettersRotatorDirective($interval, $timeout) {
    return {
      restrict: 'EA',
      replace: false,
      link: function (scope, element) {
        var $items = element.find('span');
        var delay = 100;
        var z;

        element
          .on('mouseenter', function () {
            if (z === $items.length || z == undefined) {
              rotate();
            }
          })
          .on('mouseleave', function () {
            $items.addClass('white');
          });

        function rotate() {
          $items.removeClass('rotate white');
          z = 0;

          angular.forEach($items, function (item, i) {
              $timeout(function () {
                $(item).addClass('rotate');
                z++;
              }, i * delay);
          });
        }

      }
    };
  }
})();

(function () {
  'use strict';

  angular
    .module('photo')
    .directive('scrollToTop', scrollToTopDirective);

  function scrollToTopDirective() {
    return {
      restrict: 'EA',
      replace: true,
      link: function ($scope, element) {
        var $window = $(window);

        $window.on('scroll', function() {
          if ($window.scrollTop() > 200) {
            element.fadeIn(200);
          } else {
            element.fadeOut(200);
          }
        });

        element.on('click', function () {
          $('html,body').animate({ scrollTop: 0 }, 'slow');
        })
      }
    };
  }
})();

(function () {
  'use strict';

  angular
    .module('photo')
    .factory('$templateRequest', TemplateRequestService);

  TemplateRequestService.$inject = ['$templateCache', '$http', '$q'];
  function TemplateRequestService($templateCache, $http, $q) {
    function handleRequestFn(tpl, ignoreRequestError) {
      var self = handleRequestFn;
      self.totalPendingRequests++;

      return $http.get(tpl, { cache : $templateCache })
        .then(function(response) {
          var html = response.data;
          if(!html || html.length === 0) {
            return handleError();
          }

          self.totalPendingRequests--;
          $templateCache.put(tpl, html);
          return html;
        }, handleError);

      function handleError() {
        self.totalPendingRequests--;
        if (!ignoreRequestError) {
          throw $compileMinErr('tpload', 'Failed to load template: {0}', tpl);
        }
        return $q.reject();
      }
    }

    handleRequestFn.totalPendingRequests = 0;

    return handleRequestFn;
  }
})();

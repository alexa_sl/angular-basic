angular
  .module('photo')
  .controller('LoginCtrl', LoginCtrl);

LoginCtrl.$inject = ['$scope', 'UserService', '$window', 'AuthGlobal', '$state'];

function LoginCtrl($scope, UserService, $window, AuthGlobal, $state) {
  console.log('login Ctrl!', $scope);

  $scope.form = {
    username: '',
    password: '',
    errors: {
      username: 'Минимальная длина имени - 3 символа',
      password: 'Минимальная длина пароля - 3 символа',
      response: ''
    }
  };

  $scope.loginUser = function () {
    console.log('login!' );
    UserService.login(
      $scope.form.username,
      $scope.form.password
    ).then(
      function(){
        $state.go('works');
      },
      function(error){
        $scope.form.errors.response = error;
      });
  };

  $scope.logout = function () {
    $window.sessionStorage.token = '';
    AuthGlobal();
    $state.reload();
  }
}

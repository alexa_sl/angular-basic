angular
  .module('photo')
  .factory('AuthService', AuthService)
  .factory('AuthGlobal', AuthGlobal);

AuthService.$inject = ['$window'];
AuthGlobal.$inject = ['$window', 'AuthService', '$rootScope'];

function AuthService($window) {
  var auth = {
    isLogged: $window.sessionStorage.token ? !!$window.sessionStorage.token.length : false
  };
  //console.log('AuthService', !!$window.sessionStorage.token.length);
  return auth;
}

function AuthGlobal($window, AuthService, $rootScope) {

  return function () {

    if (!!$window.sessionStorage.token.length) {
      console.log('token set!');
      AuthService.isLogged = true;
      $rootScope.$emit('auth:loggedIn');
    } else {
      AuthService.isLogged = false;
    }
    //console.log('AuthGlobal', !!$window.sessionStorage.token.length, AuthService.isLogged);
    return AuthService.isLogged;
  }
}
